const express = require('express');
const socket = require('socket.io');
const cors = require('cors');
const path = require('path');
var users = {};

const app = express();
app.use(express.static('public'));

const server = app.listen(3000, ()=> {
    console.log("listening to port 3000");
});

const io = socket(server);
io.on('connection', socket=>{
    console.log('connected id ' + socket.id);

    socket.on('new-message', function(data){
        data.nickname = socket.nickname;
        io.sockets.emit('new-message', data);  // send msg to everyone including sender
        // socket.broadcast.emit('new-message', data);  // send msg to everyone except sender
    });

    socket.on('username', function(data, callback){
        if(data in users) {
            callback(false);
        } else{
            callback(true);
            socket.nickname = data;
            users[socket.nickname] = socket;
            updateNicknames();
        }
    });

    function updateNicknames(){
        io.sockets.emit('users', Object.keys(users));
    }

    socket.on('disconnect', function(data){
        if(!socket.nickname) return;
        delete users[socket.nickname];
        updateNicknames();
    });

    socket.on('typing', function(data) {
        socket.broadcast.emit('typing', socket.nickname);
    });
});