jQuery(function($){
    var socket = io.connect();
    var chat_win_wrap = $('#chat_window_wrapper');
    var chatWin = $('#chat-window');
    var message = $('#message');
    var sendMsg = $('#send-msg');

    var nickname_box = $('#nickname_box');
    var nickname_form = $('#nickname_form');
    var nickname_err = $('#nickname_err');
    var nickname = $('#nickname');

    var users_box = $('#users');
    var feedback = $('#feedback');

    $(sendMsg).click(function(e){
        e.preventDefault();
        socket.emit('new-message', {
            message: $(message).val(),
        });
        $(message).val('');
    });

    $(nickname_form).submit(function(e){
        e.preventDefault();
        socket.emit('username', $(nickname).val(), function(data) {
            if(data){
                $(nickname_box).hide();
                $(chat_win_wrap).show();
            } else{
                $(nickname_err).html('<b>nickname is already taken..try something else</b>');
            }
        });
        $(nickname).val('');
    });

    $(message).keyup(function(){
        socket.emit('typing', true);
    });

    socket.on('new-message', function(data){
        console.log(data);
        $(feedback).text('');
        let newmsg = $('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar">'+ data.nickname + '</div><div class="col-md-10 col-xs-10"><div class="messages msg_receive"><p>' + data.message + '</p></div></div></div>');
        $(chatWin).append(newmsg);
    });

    socket.on('users', function(data){
        var html = '';
        for( i=0; i<data.length; i++ ){
            html += data[i]+'</br>';
        }
        $(users_box).html(html);
    });

    socket.on('typing', function(username){
        $(feedback).text(username + ' typing..');
    });
    
    //console.log(socket);
});